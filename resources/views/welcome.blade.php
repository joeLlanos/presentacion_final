<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Clinica Dental</title>

        <!-- Font Awesome icons (free version)-->
        
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://kit.fontawesome.com/e865f50216.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" href="{{mix('/css/app.css')}}">
        
    </head>
    <body>
        <div id="app">

        </div>
        <script src="{{ mix('js/app.js')}}"></script>  
        
    </body>
</html>
