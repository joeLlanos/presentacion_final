import axios from 'axios'
import { data } from 'jquery'

export default {
    namespaced:true,
    state: {
        allcategorys: []
    },
    getters: {
        get_all_category(state) {
            return state.allcategorys
        }
    },
    mutations: {
        SET_ALL_CATEGORY(state, value) {
            state.allcategorys = value
        }
    },
    actions: {
        async getCategorysAll({dispatch}) {
            console.log('solicitando todas la categorias')
            await axios.get('/sanctum/csrf-cookie')
            const ct = await axios.get('/api/categorys')
            console.log(ct)
            return dispatch('meCategory', ct.data)
        },

        async createCategory({dispatch}, newCategory) {
            console.log('creando nueva categoria')
            console.log(data)
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/api/categorys',newCategory)
        },
        meCategory({commit}, data) {
            console.log(data)
            commit('SET_ALL_CATEGORY', data)
        }
    }
}